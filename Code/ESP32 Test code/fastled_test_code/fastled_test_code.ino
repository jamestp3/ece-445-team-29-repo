#include <FastLED.h>
#include "charToArrCode.h"
#include "BluetoothSerial.h"


#define LED_TYPE WS2811
#define NUM_LEDS 224
#define DATA_PIN 18
#define ASCII_OFFSET 32


CRGB leds[NUM_LEDS];

BluetoothSerial SerialBT;

String message = "";
char incomingChar;

void setup() {
  Serial.begin(115200);
  SerialBT.begin("ESP32test"); //Bluetooth device name
  Serial.println("The device started, now you can pair it with bluetooth!");
  
  FastLED.addLeds<LED_TYPE, DATA_PIN>(leds, NUM_LEDS);
}


int i =0;
int j =0;

int text = 32;
  
void loop() {

    if (SerialBT.available()) {
    char incomingChar = SerialBT.read();
    if (incomingChar != '\n'){
      message += String(incomingChar);      
    }

else{

//scroll through message
while( j> (int)(message.length() * -6)){
   FastLED.clear();
   for(int k = 0; k < message.length(); k++){//each letter in string
    for (int row = 0; row < 8; row++) {
    for (int col = 0; col < 8; col++) {
      if (charToArr[message.charAt(k)-ASCII_OFFSET][row][col] == 1) {
        uint8_t ledIdx = coordToLedIndex(row +i, col + j + 6*k, 8);
        leds[ledIdx] = CRGB::Red; 
      }
    }
  }
  
}

j-=1;
FastLED.delay(100);

}

  

message = "";
j=16; 
}  
  
  }
 FastLED.show();


 
}


// connects in bottom right corner, square grid
uint8_t coordToLedIndex(uint8_t row, uint8_t col, uint8_t gridSize) {
  // Checking constraints
  //assert(row < gridSize && col < gridSize);

  if(row >gridSize-1 || col> gridSize-1){
    return -1;
  }
  
  // if it is in an even row, idx increments left to right
  // if it is odd, decrements left to right
  uint8_t rowOffset;
  uint8_t colOffset;
  
  if (row % 2) { // if it is an odd row
    rowOffset = (gridSize - row) * gridSize - 1;
    colOffset = col * -1;
  } else {
    rowOffset = (gridSize - row - 1) * gridSize;
    colOffset = col;
  }

  return rowOffset + colOffset;
}
