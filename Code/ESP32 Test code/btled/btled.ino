#include "BluetoothSerial.h"

#if !defined(CONFIG_BT_ENABLED) || !defined(CONFIG_BLUEDROID_ENABLED)
#error Bluetooth is not enabled! Please run `make menuconfig` to and enable it
#endif

#include <FastLED.h>


#define NUM_LEDS 100
#define DATA_PIN 18

CRGB leds[NUM_LEDS];

BluetoothSerial SerialBT;

void setup() {
  Serial.begin(115200);
  SerialBT.begin("ESP32test"); //Bluetooth device name
  Serial.println("The device started, now you can pair it with bluetooth!");
  FastLED.addLeds<WS2811, DATA_PIN>(leds, NUM_LEDS);
}
int text = 0;
void loop() {
  leds[30] = CRGB::Red;
  
  if (Serial.available()) {
    SerialBT.write(Serial.read());
  }
  if (SerialBT.available()) {
    
    text = SerialBT.read();
    Serial.write(text);
    
    if(text == 'b'){
     leds[2] = CRGB::Blue;
    Serial.write("blue");
    }
    else if(text == 'r'){
     leds[2] = CRGB::Red;
    }
    else if(text == 'g'){
     leds[2] = CRGB::Green; 
    }
    text= 0;
  }
  FastLED.show();
  delay(20);
}
