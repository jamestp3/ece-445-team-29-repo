#include <Wire.h>
#include "characters.h"
#include <FastLED.h>
#include "charToArrCode.h"
#include "BluetoothSerial.h"
#include <I2C_Anything.h>

#define I2C_DEV_ADDR 0x01
#define TEST_ADDR 0x02

#define LED_TYPE WS2811
#define NUM_LEDS 64
#define DATA_PIN 18
#define ASCII_OFFSET 32
#define NUM_BOARDS 4

CRGB leds[NUM_LEDS];
CRGB leds2[NUM_LEDS];
CRGB leds3[NUM_LEDS];

CRGB * boards[NUM_BOARDS] ={leds,leds2,leds3,NULL};

int addrs[NUM_BOARDS] = {0,1,2,0};

BluetoothSerial SerialBT;

uint32_t i = 0;

int prog =0;

void setup() {
  Serial.begin(115200);
  Serial.setDebugOutput(true);
  //i2c
  Wire.setClock(400000);
  Wire.begin();
  //bluetooth
  SerialBT.register_callback (Bt_Status);
  SerialBT.begin("ESP32test"); //Bluetooth device name
  Serial.println("The device started, now you can pair it with bluetooth!");
  //leds

  FastLED.addLeds<LED_TYPE, DATA_PIN>(leds, NUM_LEDS);
  FastLED.setBrightness(10);
}

//bt vars
String message = "";
char incomingChar;


int j =0;


void loop() {
    
    if (SerialBT.available()) {
      char incomingChar = SerialBT.read();
      if (incomingChar != '\n'){
        message += String(incomingChar);      
      }
    else{
      Serial.println(message);
      Serial.println(message.length());
  
      String cmd = message.substring(0, message.length() - 1);
      Serial.println(message);
      
       if(cmd== "!q"){
        SerialBT.println("Exiting...");
        prog =0;
        }
        
       if(prog ==0){
          if(cmd =="txt1"){
            SerialBT.println("Side scroll text mode:");
            prog= 1;
          }
          else if(cmd =="txt2"){
            SerialBT.println("Upward scroll text mode:");
            prog= 2;
          } 
          else if(cmd =="txt3"){
            SerialBT.println("Side scroll looped text mode");
            SerialBT.println("Type any char to stop:");
            prog= 3;
          } 
          else if(cmd =="txt4"){
            SerialBT.println("Side scroll looped text mode");
            SerialBT.println("Type any char to exit:");
            prog= 4;
          } 
           else if(cmd =="ball"){
            SerialBT.println("Ball mode");
            SerialBT.println("Type any char to exit:");
            prog= 5;
            ball();
          } 
          else if(cmd =="color"){
            SerialBT.println("Color Palette mode");
            SerialBT.println("Type any char to exit:");
            prog= 5;
            message="";
            colorPalette();
          } 
        }

        else if(prog ==1){
          displayTextSide(message);
        }

       else if(prog ==2){
        displayTextUp(message);
       }
       else if(prog ==3){
        loopTextSide(message);
       }
       else if(prog ==4){
        loopTextUp(message);
       }
       else if(prog ==5){
        //ball();
       }
       else{
        SerialBT.println("Command not valid");
       }
      message="";
      Serial.println(prog);
      }
    }
  }


void colorPalette(){

  CRGBPalette16 currentPalette[4] = {RainbowColors_p,RainbowStripeColors_p,CloudColors_p,PartyColors_p};
  TBlendType    currentBlending;
    
 
  currentBlending = LINEARBLEND;
    
  uint8_t brightness = 255;
  static uint8_t startIndex = 0;
  int pal = 0;
  colorloop:
  while(!SerialBT.available()){

    
    for(int x =0; x< NUM_BOARDS; x++){
      if(boards[x] != NULL){
        for( int i = 0; i < NUM_LEDS; ++i) {
          boards[x][i] = ColorFromPalette( currentPalette[pal], startIndex, brightness, currentBlending);
          startIndex += 3;
          }
    }
  }
  startIndex++;
  sendLedData(boards[1],1);
  FastLED.show();
  //FastLED.delay(600);
  
  }
  while (SerialBT.available()) {
      char incomingChar = SerialBT.read();
      if (incomingChar != '\n'){
        message += String(incomingChar);      
  }
  else{
    Serial.println(message);
    if(message.substring(0, message.length() - 1) == "next"){
      message ="";
      pal++;
      pal = pal%4;
      goto colorloop;
    }
    else{
      message ="";
      break;
    }
   
  }
  }

   clearAndRet();
}


void ball(){
  double x =0;
  double y= 0;

  double xspeed =1;
  double yspeed =1;

  int xBound =8;
  int yBound =8;

  while(!SerialBT.available()){
    
    int ix = (int) (x+ xspeed);
    int iy = (int) (y+ yspeed);

    if(ix<0){
      double rando = (double) random(50,100);
      rando = rando/100;
      xspeed =  rando;
    }
    if(ix>=xBound){
      double rando = (double) random(50,100);
      rando = rando/100;
      xspeed = -1.0 * rando;
    }

    if(iy<0){
      double rando = (double) random(50,100);
      rando = rando/100;
      yspeed = rando;
    }
    if(iy>=yBound){
      double rando = (double) random(50,100);
      rando = rando/100;
      yspeed = -1.0 * rando;
    }

    x+=xspeed;
    y+=yspeed;

    FastLED.clear();
    uint8_t ledIdx = coordToLedIndex((int) x, (int) y, 8);
    leds[ledIdx]= CRGB::Blue;
    FastLED.delay(500);
    FastLED.show();
    
  }

  clearAndRet();
  
}


void loopTextSide(String message){

  while(!SerialBT.available()){
    displayTextSide(message);
  }
  clearAndRet();
}

void loopTextUp(String message){

  while(!SerialBT.available()){
    displayTextUp(message);
  }
  clearAndRet();
}

void clearAndRet(){
    for(int i =0; i< NUM_BOARDS; i++){
    if(boards[i] != NULL){
      if(i==0){
        FastLED.clear();
        FastLED.show();
            }
      else{
      memset(boards[i], 0, sizeof(leds));
      sendLedData(boards[i],i);
            }
    }
  }
  prog=0;
  while(SerialBT.available()){
    char throwaway = SerialBT.read();
  }
  SerialBT.println("Exiting...");
}

void displayTextSide(String message){
      j= 8;
      while( j> (int)(message.length() * -6 -1)){

        for(int i =0; i< NUM_BOARDS; i++){
          if(boards[i] != NULL){
            memset(boards[i], 0, sizeof(leds));
            setTextSide(boards[i],j,message, i*7);
          }
        }
        sendAllLedData();
        j-=1;
        }
}

void displayTextUp(String message){
      j= 8;
      while( j> (int)(message.length() * -8 -1)){

        for(int i =0; i< NUM_BOARDS; i++){
          if(boards[i] != NULL){
            memset(boards[i], 0, sizeof(leds));
            setTextUp(boards[i],j,message, i*7);
            if(i==0){
              FastLED.show();
            }
            else{
              sendLedData(boards[i],i);
            }
          }
        }
        
        j-=1;
        //FastLED.delay(200);
        }
}


void setTextSide(CRGB ledData[], int j, String message, int offset){

          for(int k = 0; k < message.length(); k++){//each letter in string
            for (int row = 0; row < 8; row++) {
              for (int col = 0; col < 8; col++) {
                if (charToArr[message.charAt(k)-ASCII_OFFSET][row][col] == 1) {
                  
                  uint8_t ledIdx = coordToLedIndex(row, col + j + 6*k +offset, 8);
                  
                  ledData[ledIdx] = CRGB::Red; 
                }
              }
            }
          }
  
}

void setTextUp(CRGB ledData[], int j, String message, int offset){

          for(int k = 0; k < message.length(); k++){//each letter in string
            for (int row = 0; row < 8; row++) {
              for (int col = 0; col < 8; col++) {
                if (charToArr[message.charAt(k)-ASCII_OFFSET][row][col] == 1) {
                  
                  uint8_t ledIdx = coordToLedIndex(row+ j + 6*k +offset, col, 8);
                  
                  ledData[ledIdx] = CRGB::Red; 
                }
              }
            }
          }
  
}

void sendLedData(CRGB ledData[], int addr){
  for( int i= 0; i < NUM_LEDS ; i++){
        Wire.beginTransmission(addr);
        I2C_writeAnything(i);
        I2C_writeAnything(ledData[i]);
        uint8_t error = Wire.endTransmission(true);
        //Serial.printf("endTransmission: %u\n", error);
        delay(1);  
 }
}

void sendAllLedData(){

  for( int i= 0; i < NUM_LEDS ; i++){

      for( int x =1; x<NUM_BOARDS; x++){
  
        if(boards[x] != NULL){
          Wire.beginTransmission(x);
          I2C_writeAnything(i);
          I2C_writeAnything(boards[x][i]);
          uint8_t error = Wire.endTransmission(true);
          //Serial.printf("endTransmission: %u\n", error);
           delay(2);
          }
      }

}
FastLED.show();

}

// connects in bottom right corner, square grid
uint8_t coordToLedIndex(uint8_t row, uint8_t col, uint8_t gridSize) {
  // Checking constraints
  //assert(row < gridSize && col < gridSize);

  if(row >gridSize-1 || col> gridSize-1){
    return -1;
  }
  
  // if it is in an even row, idx increments left to right
  // if it is odd, decrements left to right
  uint8_t rowOffset;
  uint8_t colOffset;
  
  if (row % 2) { // if it is an odd row
    rowOffset = (gridSize - row) * gridSize - 1;
    colOffset = col * -1;
  } else {
    rowOffset = (gridSize - row - 1) * gridSize;
    colOffset = col;
  }

  return rowOffset + colOffset;
}


void Bt_Status(esp_spp_cb_event_t event, esp_spp_cb_param_t *param){
  if(event == ESP_SPP_SRV_OPEN_EVT){
    delay(100);
    Serial.println("connected");
        SerialBT.println("Welcome, please select mode:");
        SerialBT.println("txt1 -- Sideways scrolling text");
        SerialBT.println("txt2 -- Upward scrolling text");
        SerialBT.println("txt3 -- Sideways looping text");
        SerialBT.println("txt4 -- Upward looping text");
        SerialBT.println("ball -- Ball mode");
        SerialBT.println("!q -- Quit to menu");
  }
}
