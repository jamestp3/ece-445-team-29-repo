#include <Wire.h>
#include <FastLED.h>
#include "charToArrCode.h"
#include <I2C_Anything.h>

#define I2C_DEV_ADDR 0x01

uint32_t i = 0;
int counter= 0;
const int ledPin =5;

#define LED_TYPE WS2811
#define NUM_LEDS 64
#define DATA_PIN 26
#define ASCII_OFFSET 32

CRGB leds[NUM_LEDS];

String message = "";
char incomingChar;

void onRequest(){
  Wire.print(i++);
  Wire.print(" Packets.");
  Serial.println("onRequest");
}

byte packet[4];

int x;

void onReceive(int len){
  Serial.printf("onReceive[%d]: \n", len);

  if(len>= (sizeof(x)+ sizeof(leds[0]))){
    I2C_readAnything(x);
    if(x>=NUM_LEDS || x<0){
      x=0;
    }
    Serial.println(x);
    I2C_readAnything(leds[x]);
    if(x ==63){
      FastLED.show();
    }
    
  }
}



void setup() {
  Serial.begin(115200);
  Serial.setDebugOutput(true);
  Wire.onReceive(onReceive);
  Wire.onRequest(onRequest);
  Wire.begin((uint8_t)I2C_DEV_ADDR);
 
  pinMode (ledPin, OUTPUT);
  FastLED.addLeds<LED_TYPE, DATA_PIN>(leds, NUM_LEDS);
  FastLED.setBrightness(  10 );
#if CONFIG_IDF_TARGET_ESP32
  char message[64];
  snprintf(message, 64, "%u Packets.", i++);
  Wire.slaveWrite((uint8_t *)message, strlen(message));
#endif
}

void loop() {
}
