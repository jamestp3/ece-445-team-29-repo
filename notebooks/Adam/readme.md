# 02/06/2022
### Project Proposal

Today, we finalized and reviewed our project proposal together before the submission. We plan on making a modular LED board that can be used as a wall decoration. 

Commercially available LED strips have limited customizability and can only be used in a long strip and not in a grid. Similarly, decorative tiles like nanoleaf can only display a single color and is not very customizable nor display any special effects or texts. 

We aim to have a main panel that provides power and communication with external devices and have additional panels that connect to the main tile. 


# 02/22/2022
### Design Document Draft

Our goal for the day is the revisit our project proposal and complete our final design document and prepare our requirements and plans. The most important key points are listed below:

* The panel will be able to display text, images, and various effects that will dynamically adapt to the current arrangement. We are thinking of either a ping pong like ball bouncing around the display, or a snake to demonstrate effects while showing that the display is connected the right way. 
* The core panel will be able to recognize and detect the current configuration, and will be in charge of all the communication with a Bleutooth–connected device in addition to handling and controlling the display data for the additional expansion tiles. 


# 02/28/2022
### Design Document

We finalized our design documentations and our high-level requirements after some discussion in our team and the feedback from our TA. We clarified some ambiguous objectives and quantified and clarified how to objectively measure each objective. To list a few, we explicitly stated a 1 second limit for the core tile to update change in expansion tiles, and also limited the Bluetooth connection time to 7 seconds. 


# 03/02/2022

I made a preliminary version of the text array to convert ASCII characters to a 8x8 grid for our LED grid as this will be useful for initial tests and could be time consuming later. The characters has a maximum height of 8 pixels, and has a variable width such as shown below:

```
static const unordered_map<char, vector<vector<uint8_t> > > charToArr =
...
{'A', {
        {0,1,1,1,0},
        {1,0,0,0,1},
        {1,0,0,0,1},
        {1,1,1,1,1},
        {1,0,0,0,1},
        {1,0,0,0,1},
        {1,0,0,0,1},
        {0,0,0,0,0}
    }},
...
```

# 03/10/2022

We had a meeting as a team before we leave for spring break to make sure all of our orders are in, and verify we have ordred the right parts. We ordered some additional parts and connectors to Kyle's house and borrowed an unused ESP32 dev board from a friend so that we can more efficiently develop and test the software components in parallel with the hardware and PCB design. 


# 03/29/2022

Today, we used the previous code for displaying characters to do some preliminary tests displaying simple charactes and messages on the LED board. We ran across issues with the original C++ vectors and unordered maps on the ESP32, and restructured the character map to a simple C array as it will minimally effect any memory use or performance in theory. 

In addition, we wrote a method to convert a 2D coordinate to a LED number as each board has a serial LED where it starts in the bottom left and snakes around the board to make a 8 by 8 grid. 

```
// connects in bottom right corner, square grid
uint8_t coordToLedIndex(uint8_t row, uint8_t col, uint8_t gridSize) {
  // Checking constraints
  //assert(row < gridSize && col < gridSize);

  if(row >gridSize-1 || col> gridSize-1){
    return -1;
  }
  
  // if it is in an even row, idx increments left to right
  // if it is odd, decrements left to right
  uint8_t rowOffset;
  uint8_t colOffset;
  
  if (row % 2) { // if it is an odd row
    rowOffset = (gridSize - row) * gridSize - 1;
    colOffset = col * -1;
  } else {
    rowOffset = (gridSize - row - 1) * gridSize;
    colOffset = col;
  }

  return rowOffset + colOffset;
}
```


# 03/30/2022

We experimented with bluetooth paring of the ESP32 and a device we'll use to control. There were good documentation and information available online, but discovered that it is challenging to make the ESP communicate with Apple devices that most of the team members are using and appears to require Bluetooth Low Energy. We were able to use an Android device and `BluetoothSerial` module to send basic serial messages to the ESP32 and display texts continuing from yesterday's work. 


# 4/11/2022

After some research and testing, we realized that we most likely will not be able to implement the dynamically adapting panel with out current hardware and implementation. The ESP32 can only have 2 separate I2C connections running simultaneously, and if we want a module to neighboring modules in all 4 directions, we will need to separate the connections for all the neighbors. We are currently simplifying the connection to put all the data on a single I2C bus, and assigning each device an unique ID like `#define I2C_DEV_ADDR 0x01` as a compromize until we find a better solution. 

