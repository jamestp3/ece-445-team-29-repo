# 2/28/2022

Today we worked on our finalizing all aspects of our design. Our high level requirments are as follows: 
- The four panels must be able to display text, images, and dynamic effects
that can adapt to current configuration. If text is too small to fit within
boundaries, it will scroll across panels.
- "Core panel" must automatically recognize any change to the configu-
ration of expansion tiles, updating the display output to each tile to fit
within the new boundaries in under 1 second.
- Panels must be able to be controlled through Bluetooth by a smartphone
or other external device (must connect to Bluetooth device within 5-7
seconds).

We also finalzied all of our subsystems and how they will work with each other.

# 3/29/2022
Today we worked a lot on our code for displaying text. We have a prototype board that has 16x16 WS2811 LEDS. We attached an esp32 dev module to this board and started to develop. The first thing we needed to was write a function to account for the differing directions of the leds. Because we are using addressable leds, in the software the whole board is just represented by a 1d array. We need to convert between this index and an x, y value using a function. What we wrote is bellow:
```
// connects in bottom right corner, square grid
uint8_t coordToLedIndex(uint8_t row, uint8_t col, uint8_t gridSize) {
  // Checking constraints
  //assert(row < gridSize && col < gridSize);

  if(row >gridSize-1 || col> gridSize-1){
    return -1;
  }
  
  // if it is in an even row, idx increments left to right
  // if it is odd, decrements left to right
  uint8_t rowOffset;
  uint8_t colOffset;
  
  if (row % 2) { // if it is an odd row
    rowOffset = (gridSize - row) * gridSize - 1;
    colOffset = col * -1;
  } else {
    rowOffset = (gridSize - row - 1) * gridSize;
    colOffset = col;
  }

  return rowOffset + colOffset;
}
```
# 3/30/2022

Today I worked on integrating bluetooth to our current testing environment. Because we are using an esp32, bluetooth is fully integrated into the chip so proramming it isnt too hard. For bluetooth, we receive 1 char at a time and save it into a string. We then display this string on our board, scrolling through at a set rate. 

```
BluetoothSerial SerialBT;

String message = "";
char incomingChar;

void setup() {
  Serial.begin(115200);
  SerialBT.begin("ESP32test"); //Bluetooth device name
  Serial.println("The device started, now you can pair it with bluetooth!");
  FastLED.addLeds<LED_TYPE, DATA_PIN>(leds, NUM_LEDS);
}

int i =0;
int j =0;
int text = 32;

void loop() {
    if (SerialBT.available()) {
    char incomingChar = SerialBT.read();
    if (incomingChar != '\n'){
      message += String(incomingChar);      
    }
else{
//scroll through message
while( j> (int)(message.length() * -6)){
   FastLED.clear();
   for(int k = 0; k < message.length(); k++){//each letter in string
    for (int row = 0; row < 8; row++) {
    for (int col = 0; col < 8; col++) {
      if (charToArr[message.charAt(k)-ASCII_OFFSET][row][col] == 1) {
        uint8_t ledIdx = coordToLedIndex(row +i, col + j + 6*k, 8);
        leds[ledIdx] = CRGB::Red; 
      }
    }
  } 
}
j-=1;
FastLED.delay(100);
}
message = "";
j=16; 
}  
  }
 FastLED.show();

}
```
The above code is a snipet of what is running on our esp32. The code works very well, to test I connected my phone via bluetooth and send messages. The messages were able to be received and displayed by our panel.



# 4/10/2022

Today I worked on linking 2 esp32s together with i2c. The goal was to have our master send a message to the slave, and for the slave to then display the message on the led board. The master would be reciving the message from bluetooth.
I was able to get all of this working, and it worked pretty well.

# 4/11/2022
Today I worked on modifing the i2c code to be able to send led data between 2 esp32s. This proved to be very difficult, as with i2c we can only send data in bytes. Our leds are stored as a CRGB array, which is not compatable with i2c. Our current solution is to send over the RGB data for each pixel seperatly. This works with small amounts of pixel data, but when we try to send over moving images we have significant fragmenting. We need to look for another solution to sending this data over. 

```
#include <Wire.h>
#include <FastLED.h>
#include "charToArrCode.h"


#define I2C_DEV_ADDR 0x01

uint32_t i = 0;
int counter= 0;
const int ledPin =5;

#define LED_TYPE WS2811
#define NUM_LEDS 224
#define DATA_PIN 18
#define ASCII_OFFSET 32

CRGB leds[NUM_LEDS];

String message = "";
char incomingChar;

void onRequest(){
  Wire.print(i++);
  Wire.print(" Packets.");
  Serial.println("onRequest");
}

byte packet[4];

void onReceive(int len){
  Serial.printf("onReceive[%d]: \n", len);

   while(Wire.available()){
    
    for(int i=0; i<len; i++)
    {
      packet[i] = Wire.read();
      Serial.print(packet[i], HEX);
    }
    if(len ==4){
      int index = packet[0];
      leds[index].r = packet[1];
      leds[index].g = packet[2];
      leds[index].b = packet[3];
      FastLED.show();
    }
  
  } 
}



void setup() {
  Serial.begin(115200);
  Serial.setDebugOutput(true);
  Wire.onReceive(onReceive);
  Wire.onRequest(onRequest);
  Wire.begin((uint8_t)I2C_DEV_ADDR);
  pinMode (ledPin, OUTPUT);
  FastLED.addLeds<LED_TYPE, DATA_PIN>(leds, NUM_LEDS);
  
#if CONFIG_IDF_TARGET_ESP32
  char message[64];
  snprintf(message, 64, "%u Packets.", i++);
  Wire.slaveWrite((uint8_t *)message, strlen(message));
#endif
}

void loop() {
}
```


```


#include <Wire.h>
#include "characters.h"
#include <FastLED.h>
#include "charToArrCode.h"
#include "BluetoothSerial.h"
#include <I2C_Anything.h>


#define I2C_DEV_ADDR 0x01


#define LED_TYPE WS2811
#define NUM_LEDS 64
#define DATA_PIN 18
#define ASCII_OFFSET 32


CRGB leds[NUM_LEDS];

BluetoothSerial SerialBT;

uint32_t i = 0;


void setup() {
  Serial.begin(115200);
  Serial.setDebugOutput(true);
  Wire.begin();
  SerialBT.begin("ESP32test"); //Bluetooth device name
  Serial.println("The device started, now you can pair it with bluetooth!");
  
  FastLED.addLeds<LED_TYPE, DATA_PIN>(leds, NUM_LEDS);
}
String message = "";
char incomingChar;
int j =0;

byte packet[4];

void loop() {

    if (SerialBT.available()) {
      char incomingChar = SerialBT.read();
      if (incomingChar != '\n'){
        message += String(incomingChar);      
      }
    else{
      Serial.println(message);

    
      while( j> (int)(message.length() * -6)){
   FastLED.clear();
   for(int k = 0; k < message.length(); k++){//each letter in string
    for (int row = 0; row < 8; row++) {
    for (int col = 0; col < 8; col++) {
      if (charToArr[message.charAt(k)-ASCII_OFFSET][row][col] == 1) {
        uint8_t ledIdx = coordToLedIndex(row, col + j + 6*k, 8);
        leds[ledIdx] = CRGB::Red; 
      }
    }
  }
}

j-=1;
FastLED.delay(2000);
sendLedData(leds);
}

      

      
      message="";
      j=16; 
    }
    }



}


void sendLedData(CRGB ledData[]){

for( int i= 0; i < NUM_LEDS ; i++){
        packet[0] = i;
        packet[1] = leds[i].r;
        packet[2] = leds[i].g;
        packet[3] = leds[i].b;


        Wire.beginTransmission(I2C_DEV_ADDR);
        for(int x = 0; x<4;x++){
          Wire.write(packet[x]);
        }
        uint8_t error = Wire.endTransmission(true);
        Serial.printf("endTransmission: %u\n", error);
 
        
 }

  
}



// connects in bottom right corner, square grid
uint8_t coordToLedIndex(uint8_t row, uint8_t col, uint8_t gridSize) {
  // Checking constraints
  //assert(row < gridSize && col < gridSize);

  if(row >gridSize-1 || col> gridSize-1){
    return -1;
  }
  
  // if it is in an even row, idx increments left to right
  // if it is odd, decrements left to right
  uint8_t rowOffset;
  uint8_t colOffset;
  
  if (row % 2) { // if it is an odd row
    rowOffset = (gridSize - row) * gridSize - 1;
    colOffset = col * -1;
  } else {
    rowOffset = (gridSize - row - 1) * gridSize;
    colOffset = col;
  }

  return rowOffset + colOffset;
}
```



# 4/16/2022

Today I worked more on the i2c code. The goal is to be able to send full led data from the controller esp to the peripherial esp. I was able to get this working today by using a library called I2C_Anywhere to send the CRGB data type. I also needed to configure the speeds of the bus, as well as delays between each transmission. Currently the controller sends data one led at a time, with a delay of 5ms to reduce data loss over i2c.
```
void sendLedData(CRGB ledData[]){

for( int i= 0; i < NUM_LEDS ; i++){

        Wire.beginTransmission(I2C_DEV_ADDR);
        I2C_writeAnything(i);
        I2C_writeAnything(ledData[i]);
        uint8_t error = Wire.endTransmission(true);
        //Serial.printf("endTransmission: %u\n", error);
        delay(5);    
 }
```
Because of this, you could see each pixel change as data came into the peripheral board. I added an additional condition to the peripheral such that it only updates when the 64th (index of 63) led is sent over. This resulted in a very smooth text scrolling animation between the two boards. This is one of the most challenging aspects of our project, but it seems that we've made good progress with it. We still need to try using more boards, and set configuation patterns, but we at least now have a means to do so. 
![Multi-board text scroll]( 20220416_175121.jpg)